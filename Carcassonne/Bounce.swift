//
//  Bounce.swift
//  Carcassonne
//
//  Created by Matthieu LEFÈVRE on 22/01/2022.
//

import Foundation
import SwiftUI

struct ImageBackgroundView: View {
    
    @State var animate: Bool = false
    let animation: Animation = Animation.linear(duration: 50.0).repeatForever(autoreverses: true)
    
    var body: some View {
        GeometryReader { geo in
            HStack(spacing: -1) {
                Image("bg")
                    .aspectRatio(contentMode: .fit)

                Image("bg")
                    .aspectRatio(contentMode: .fit)
                    .frame(width: geo.size.width, alignment: .leading)
            }
            .frame(width: geo.size.width+500, height: geo.size.height,
                   alignment: animate ? .trailing : .leading)
        }
        .ignoresSafeArea()
        .onAppear {
            withAnimation(animation) {
                animate.toggle()
            }
        }
    }
}

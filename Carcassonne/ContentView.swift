//
//  ContentView.swift
//  Carcassonne
//
//  Created by Matthieu LEFÈVRE on 22/01/2022.
//

import SwiftUI


struct ContentView: View {
    
    var body: some View {
        Start()
    }
    
}

struct Start: View {
    @State var b_play = false
    @State var b_bg = false
    @State var first_view = 1
    
    var body: some View {
        ZStack {
            Color.white
                .ignoresSafeArea()
            if first_view < 2 {
                VStack {
                    Image("titre")
                        .resizable()
                        .scaledToFit()
                        .padding(30)
                        .padding(.bottom,b_play ? 300 : 15 )
                    if b_play {
                        Button {
                            withAnimation(.easeInOut(duration: 2)) {
                                first_view+=1
                            }
                        } label: {
                            Image("play")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 100)
                        }
                    }
                }
                .padding(0)
                .background {
                    if b_bg {
                        ImageBackgroundView()
                    }
                }
                .zIndex(1)
                
            }
        }
        .onAppear {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            AppDelegate.orientationLock = .portrait
            
            withAnimation(.easeInOut(duration: 2)) {
                b_bg.toggle()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                withAnimation(.spring(response: 1.5, dampingFraction: 1, blendDuration: 1)) {
                    b_play.toggle()
                }
            }
        }
    }
}

struct MenuA: View {
    var body: some View {
        HStack {
            Text("Test")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
